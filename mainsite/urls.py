from . import views
from django.urls import path
from user import views as user_views

urlpatterns = [
    path ('', views.home, name='site-home'),
    path ('about/', views.about, name='site-about'),
    path ('contact/', views.contact, name='site-contact'),
    path ('register/', user_views.register, name='register'),
]