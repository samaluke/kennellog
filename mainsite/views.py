from django.shortcuts import render

def home(request):
    context = {
        'Title': 'Home'
    }
    return render(request, 'mainsite/home.html', context)

def about (request):
    context = {
        'Title': 'About'
    }
    return render(request, 'mainsite/about.html', context)

def contact (request):
    context = {
        'Title': 'Contact'
    }
    return render(request, 'mainsite/contact.html', context)