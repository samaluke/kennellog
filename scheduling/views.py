from django.shortcuts import render, get_object_or_404
from user.models import DogProfile
from .models import (
    DaycareReservation,
    BoardingReservation,
)
from django.contrib.auth.models import User
from datetime import date, datetime
from django.views.generic import DetailView, UpdateView, RedirectView, CreateView, DeleteView
from django.utils.text import slugify
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
import uuid


class BoardingReservationCreateView (LoginRequiredMixin, CreateView):
    model = BoardingReservation
    fields = ['dog', 'facility', 'startDate', 'endDate', 'dropoffTime', 'pickupTime', 'notes']

    def form_valid (self, form):
        form.instance.customer = self.request.user
        form.instance.slug = slugify (form.instance.dog.nickName + self.request.user.username + uuid.uuid4().hex[:6].upper())
        return super().form_valid (form)

class BoardingReservationUpdateView (LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = BoardingReservation
    fields = ['dog', 'facility', 'startDate', 'endDate', 'dropoffTime', 'pickupTime', 'notes']

    def form_valid (self, form):
        return super().form_valid(form)

    def test_func (self):
        return True

class BoardingReservationDeleteView (LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = BoardingReservation
    success_url = ''

    def test_func (self):
        return True