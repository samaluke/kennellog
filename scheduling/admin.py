from django.contrib import admin
from .models import DaycareReservation, BoardingReservation, EmployeeTask, Kennel, Playground

class TaskAdmin (admin.ModelAdmin) :
    list_display = ('title', 'startTime', 'date')
    search_fields = ['name',]

admin.site.register (DaycareReservation)
admin.site.register (BoardingReservation)
admin.site.register (EmployeeTask, TaskAdmin)
admin.site.register (Kennel)
admin.site.register (Playground)
