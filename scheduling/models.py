from django.db import models
from user.models import DogProfile
import django.utils.timezone
from django.contrib.auth.models import User
from django.urls import reverse
from datetime import timedelta, date, datetime, time
from django.utils.text import slugify
import uuid 


EMPLOYEE_TASK_ROLES = (
    (0,"Dog Exerciser"),
    (1,"Management"),
    (2,"Trainer"),
    (3,"Playtime Handler"),
    (4,"Kennel Wrangler"),
    (5,"No Role"),
)

DOG_STATUS = (
    (0,"GOOD"),
    (1,"ATTN"),
    (2,"URGENT"),
)

#////////////////////////////////////////////////////////////////////////////////////////////////////////////
class Playground (models.Model):
    facility = models.ForeignKey (User, on_delete=models.CASCADE, related_name='facilityOfPlayground')
    employee = models.ForeignKey (User, on_delete=models.CASCADE, related_name='employeeOfPlayground', null=True, blank=True)
    playgroundNum = models.IntegerField ()
    maxOccupants = models.IntegerField ()


#////////////////////////////////////////////////////////////////////////////////////////////////////////////
class DaycareReservation (models.Model):
    customer = models.ForeignKey (User, on_delete=models.CASCADE, related_name='customerdaycare')
    dog = models.ForeignKey (DogProfile, on_delete=models.CASCADE, related_name='dogdaycare')
    facility = models.ForeignKey (User, on_delete=models.CASCADE, related_name='facilitydaycare')
    dropoffTime = models.TimeField ()
    pickupTime = models.TimeField ()
    date = models.DateField (default=django.utils.timezone.now)
    approved = models.BooleanField (default=False)
    notes = models.TextField (max_length=1000, null=True, blank=True)
    playground = models.ForeignKey (Playground, on_delete=models.CASCADE, related_name="playgroundIn", null=True, blank=True)

    def __str__(self):
        return self.dog.nickName

    def get_approve_url (self):
        return reverse ('board-res-approval', kwargs={'slug': self.slug})

    def get_absolute_url (self):
        return reverse ('employee-admin-review-reservations')

def createDaycareCheckInTask (reservation):
    task = EmployeeTask.create (facility=reservation.facility)
    task.reservationDaycare = reservation
    task.employeeRole = 1
    task.title = "Check in " + reservation.dog.nickName
    task.description = "Check in at arrival time and bring to playroom _____."
    task.startTime = reservation.dropoffTime
    task.date = reservation.date
    task.premadeTask = True
    task.autoMakeTask = True
    task.slug = slugify (task.title + task.dog.nickName + task.facility.username + task.startTime + task.date)
    task.save ()

def createDaycareCheckOutTask (reservation):
    task = EmployeeTask.create (facility=reservation.facility)
    task.reservationBoarding = reservation
    task.employeeRole = 1
    task.title = "Check out " + reservation.dog.nickName
    task.description = "Check in at arrival time and bring to playroom _____."
    task.startTime = reservation.pickupTime
    task.date = reservation.date
    task.premadeTask = True
    task.autoMakeTask = True
    task.slug = slugify (task.title + task.dog.nickName + task.facility.username + task.startTime + task.date)
    task.save ()


#////////////////////////////////////////////////////////////////////////////////////////////////
class BoardingReservation (models.Model):
    customer = models.ForeignKey (User, on_delete=models.DO_NOTHING, related_name='customerboarding')
    dog = models.ForeignKey (DogProfile, on_delete=models.DO_NOTHING, related_name='dogboarding')
    facility = models.ForeignKey (User, on_delete=models.DO_NOTHING, related_name='facilityboarding')
    startDate = models.DateField ()
    endDate = models.DateField ()
    dropoffTime = models.TimeField ()
    pickupTime = models.TimeField ()
    approved = models.BooleanField (default=False)
    notes = models.TextField (max_length=1000, null=True, blank=True)
    slug = models.SlugField (max_length=300, unique=True, blank=True, null=True)
    kennelNum = models.IntegerField (null=True, blank=True)
    status = models.IntegerField (choices=DOG_STATUS, default=0)

    def __str__(self):
        return self.dog.nickName

    def get_kudos_url (self):
        return reverse ('board-res-approval', kwargs={'slug': self.slug})

    def get_absolute_url (self):
        return reverse ('site-home')

def createBoardPrepCheckInTask (reservation):
    task = EmployeeTask.create (facility=reservation.facility)
    task.reservationBoarding = reservation
    task.employeeRole = 4
    task.title = "Prepare kennel ___ for check in"
    task.description = "Clean out kennel if dirty, lay out blankets and provide water"
    task.startTime = reservation.dropoffTime - datetime.time(minutes=30)
    task.date = reservation.startDate
    task.premadeTask = True
    task.autoMakeTask = True
    task.slug = slugify (task.title + reservation.dog.nickName + uuid.uuid4().hex[:6].upper())
    task.save ()

def createBoardCheckInTask (reservation):
    task = EmployeeTask.create (facility=reservation.facility)
    task.reservationBoarding = reservation
    task.employeeRole = 1
    task.title = "Check in " + reservation.dog.nickName
    task.description = "Check in at arrival time and bring to kennel "
    task.startTime = reservation.dropoffTime
    task.date = reservation.startDate
    task.premadeTask = True
    task.autoMakeTask = True
    task.slug = slugify (task.title + reservation.dog.nickName + uuid.uuid4().hex[:6].upper())
    task.save ()

def daterange (start_date, end_date):
    for n in range (int ((end_date - start_date).days + 1)):
        yield start_date + timedelta (n)

def createBoardFeedTasks (reservation):
    for day in daterange (reservation.startDate, reservation.endDate):
        if (day == reservation.startDate):
            if (reservation.dog.feedTime1 > reservation.dropoffTime):
                task = EmployeeTask.create (facility=reservation.facility)
                task.reservationBoarding = reservation
                task.employeeRole = 4
                task.title = "Feed " + reservation.dog.nickName
                task.description = "Feed " + reservation.dog.nickName + " in kennel " + str(reservation.kennelNum) + " " + str(reservation.dog.feedAmt1) + " cups."
                task.startTime = reservation.dog.feedTime1
                task.date = day
                task.premadeTask = True
                task.autoMakeTask = True
                task.slug = slugify (task.title + reservation.dog.nickName + uuid.uuid4().hex[:6].upper())
                task.save ()
        elif (day == reservation.endDate):
            if (reservation.dog.feedTime1 < reservation.pickupTime):
                task = EmployeeTask.create (facility=reservation.facility)
                task.reservationBoarding = reservation
                task.employeeRole = 4
                task.title = "Feed " + reservation.dog.nickName
                task.description = "Feed " + reservation.dog.nickName + " in kennel " + str(reservation.kennelNum) + " " + str(reservation.dog.feedAmt1) + " cups."
                task.startTime = reservation.dog.feedTime1
                task.date = day
                task.premadeTask = True
                task.autoMakeTask = True
                task.slug = slugify (task.title + reservation.dog.nickName + uuid.uuid4().hex[:6].upper())
                task.save ()
        else:
            task = EmployeeTask.create (facility=reservation.facility)
            task.reservationBoarding = reservation
            task.employeeRole = 4
            task.title = "Feed " + reservation.dog.nickName
            task.description = "Feed " + reservation.dog.nickName + " in kennel " + str(reservation.kennelNum) + " " + str(reservation.dog.feedAmt1) + " cups."
            task.startTime = reservation.dog.feedTime1
            task.date = day
            task.premadeTask = True
            task.autoMakeTask = True
            task.slug = slugify (task.title + reservation.dog.nickName + uuid.uuid4().hex[:6].upper())
            task.save ()

        if (reservation.dog.feedTime2):
            if (day == reservation.startDate):
                if (reservation.dog.feedTime2 > reservation.dropoffTime):
                    task = EmployeeTask.create (facility=reservation.facility)
                    task.reservationBoarding = reservation
                    task.employeeRole = 4
                    task.title = "Feed " + reservation.dog.nickName
                    task.description = "Feed " + reservation.dog.nickName + " in kennel " + str(reservation.kennelNum) + " " + str(reservation.dog.feedAmt2) + " cups."
                    task.startTime = reservation.dog.feedTime2
                    task.date = day
                    task.premadeTask = True
                    task.autoMakeTask = True
                    task.slug = slugify (task.title + reservation.dog.nickName + uuid.uuid4().hex[:6].upper())
                    task.save ()
            elif (day == reservation.endDate):
                if (reservation.dog.feedTime2 < reservation.pickupTime):
                    task = EmployeeTask.create (facility=reservation.facility)
                    task.reservationBoarding = reservation
                    task.employeeRole = 4
                    task.title = "Feed " + reservation.dog.nickName
                    task.description = "Feed " + reservation.dog.nickName + " in kennel " + str(reservation.kennelNum) + " " + str(reservation.dog.feedAmt2) + " cups."
                    task.startTime = reservation.dog.feedTime2
                    task.date = day
                    task.premadeTask = True
                    task.autoMakeTask = True
                    task.slug = slugify (task.title + reservation.dog.nickName + uuid.uuid4().hex[:6].upper())
                    task.save ()
            else:
                task = EmployeeTask.create (facility=reservation.facility)
                task.reservationBoarding = reservation
                task.employeeRole = 4
                task.title = "Feed " + reservation.dog.nickName
                task.description = "Feed " + reservation.dog.nickName + " in kennel " + str(reservation.kennelNum) + " " + str(reservation.dog.feedAmt2) + " cups."
                task.startTime = reservation.dog.feedTime2
                task.date = day
                task.premadeTask = True
                task.autoMakeTask = True
                task.slug = slugify (task.title + reservation.dog.nickName + uuid.uuid4().hex[:6].upper())
                task.save ()

        if (reservation.dog.feedTime3):
            if (day == reservation.startDate):
                if (reservation.dog.feedTime3 > reservation.dropoffTime):
                    task = EmployeeTask.create (facility=reservation.facility)
                    task.reservationBoarding = reservation
                    task.employeeRole = 4
                    task.title = "Feed " + reservation.dog.nickName
                    task.description = "Feed " + reservation.dog.nickName + " in kennel " + str(reservation.kennelNum) + " " + str(reservation.dog.feedAmt3) + " cups."
                    task.startTime = reservation.dog.feedTime3
                    task.date = day
                    task.premadeTask = True
                    task.autoMakeTask = True
                    task.slug = slugify (task.title + reservation.dog.nickName + uuid.uuid4().hex[:6].upper())
                    task.save ()
            elif (day == reservation.endDate):
                if (reservation.dog.feedTime3 < reservation.pickupTime):
                    task = EmployeeTask.create (facility=reservation.facility)
                    task.reservationBoarding = reservation
                    task.employeeRole = 4
                    task.title = "Feed " + reservation.dog.nickName
                    task.description = "Feed " + reservation.dog.nickName + " in kennel " + str(reservation.kennelNum) + " " + str(reservation.dog.feedAmt3) + " cups."
                    task.startTime = reservation.dog.feedTime3
                    task.date = day
                    task.premadeTask = True
                    task.autoMakeTask = True
                    task.slug = slugify (task.title + reservation.dog.nickName + uuid.uuid4().hex[:6].upper())
                    task.save ()
            else:
                task = EmployeeTask.create (facility=reservation.facility)
                task.reservationBoarding = reservation
                task.employeeRole = 4
                task.title = "Feed " + reservation.dog.nickName
                task.description = "Feed " + reservation.dog.nickName + " in kennel " + str(reservation.kennelNum) + " " + str(reservation.dog.feedAmt3) + " cups."
                task.startTime = reservation.dog.feedTime3
                task.date = day
                task.premadeTask = True
                task.autoMakeTask = True
                task.slug = slugify (task.title + reservation.dog.nickName + uuid.uuid4().hex[:6].upper())
                task.save ()

def createBoardWalkTask (reservation):
    for day in daterange (reservation.startDate, reservation.endDate):
        if (day!=reservation.startDate and day!=reservation.endDate):
            task = EmployeeTask.create (facility=reservation.facility)
            task.reservationBoarding = reservation
            task.employeeRole = 0
            task.title = "Walk " + reservation.dog.nickName
            task.description = "Take " + reservation.dog.nickName + " on a 15 minute walk."
            task.date = day
            task.premadeTask = True
            task.autoMakeTask = True
            task.slug = slugify (task.title + reservation.dog.nickName + uuid.uuid4().hex[:6].upper())
            task.save ()
        elif (day==reservation.startDate and reservation.dropoffTime < time(13, 0, 0) or (day==reservation.endDate and reservation.pickupTime > time(13))):
            task = EmployeeTask.create (facility=reservation.facility)
            task.reservationBoarding = reservation
            task.employeeRole = 0
            task.title = "Walk " + reservation.dog.nickName
            task.description = "Take " + reservation.dog.nickName + " on a 15 minute walk."
            task.date = day
            task.premadeTask = True
            task.autoMakeTask = True
            task.slug = slugify (task.title + reservation.dog.nickName + uuid.uuid4().hex[:6].upper())
            task.save ()

def createBoardPottyBreakTasks (reservation):
    for day in daterange (reservation.startDate, reservation.endDate):
        if (day==reservation.startDate and reservation.dropoffTime < time(2,0,0)) or (day==reservation.endDate and reservation.pickupTime > time(2,0,0)):
            times = 1
        elif (day==reservation.startDate and reservation.dropoffTime < time(5,0,0) or (day==reservation.endDate and reservation.pickupTime < time(12,0,0))):
            times = 1
        else:
            times = 3
        for x in range(times):
            task = EmployeeTask.create (facility=reservation.facility)
            task.reservationBoarding = reservation
            task.employeeRole = 0
            task.title = reservation.dog.nickName + "'s potty break"
            task.description = "Take " + reservation.dog.nickName + " for a five minute potty break."
            task.date = day
            task.premadeTask = True
            task.autoMakeTask = True
            task.slug = slugify (task.title + reservation.dog.nickName + uuid.uuid4().hex[:6].upper())
            task.save ()

def createBoardPlayTask (reservation):
    for day in daterange (reservation.startDate, reservation.endDate):
        if (day==reservation.startDate and reservation.dropoffTime < time(2,0,0)) or (day==reservation.endDate and reservation.pickupTime > time(2,0,0)) or (day!=reservation.startDate and day!=reservation.endDate):
            task = EmployeeTask.create (facility=reservation.facility)
            task.reservationBoarding = reservation
            task.employeeRole = 0
            task.title = "Playtime with " + reservation.dog.nickName
            task.description = "Play with " + reservation.dog.nickName + " 1 on 1 for 10 minutes."
            task.date = day
            task.premadeTask = True
            task.autoMakeTask = True
            task.slug = slugify (task.title + reservation.dog.nickName + uuid.uuid4().hex[:6].upper())
            task.save ()

def createBoardPrepCheckOutTask (reservation):
    task = EmployeeTask.create (facility=reservation.facility)
    task.reservationBoarding = reservation
    task.employeeRole = 4
    task.title = "Prepare kennel ___ for check out"
    task.description = "Gather personal items and prepare for check out."
    task.startTime = reservation.dropoffTime - time(00,30,00)
    task.date = reservation.endDate
    task.premadeTask = True
    task.autoMakeTask = True
    task.slug = slugify (task.title + reservation.dog.nickName + uuid.uuid4().hex[:6].upper())
    task.save ()

def createBoardCheckOutTask (reservation):
    task = EmployeeTask.create (facility=reservation.facility)
    task.reservationBoarding = reservation
    task.employeeRole = 1
    task.title = "Check out " + reservation.dog.nickName
    task.description = "Check out at departure time and bring to owner "
    task.startTime = reservation.pickupTime
    task.date = reservation.endDate
    task.premadeTask = True
    task.autoMakeTask = True
    task.slug = slugify (task.title + reservation.dog.nickName + uuid.uuid4().hex[:6].upper())
    task.save ()

def createBoardCleanTask (reservation):
    task = EmployeeTask.create (facility=reservation.facility)
    task.reservationBoarding = reservation
    task.employeeRole = 4
    task.title = "Clean kennel ____"
    task.description = "Clean out the kennel after checkout and organize/tidy."
    task.startTime = reservation.dropoffTime + timedelta (minutes=30)
    task.date = reservation.endDate
    task.premadeTask = True
    task.autoMakeTask = True
    task.slug = slugify (task.title + reservation.dog.nickName + uuid.uuid4().hex[:6].upper())
    task.save ()


#///////////////////////////////////////////////////////////////////////////////////////////////////
class EmployeeTask (models.Model):
    employee = models.ForeignKey (User, on_delete=models.CASCADE, related_name='employeeOfTask', null=True, blank=True)
    facility = models.ForeignKey (User, on_delete=models.CASCADE, related_name='facilityOfTask', null=True, blank=True)
    reservationDaycare = models.ForeignKey (DaycareReservation, on_delete=models.CASCADE, related_name='daycareRes', null=True, blank=True)
    reservationBoarding = models.ForeignKey (BoardingReservation, on_delete=models.CASCADE, related_name='boardingRes', null=True, blank=True)
    employeeRole = models.IntegerField (choices=EMPLOYEE_TASK_ROLES, default=5)
    title = models.CharField (max_length=400)
    description = models.CharField (max_length=1000)
    timeSensitive = models.BooleanField (default=False)
    startTime = models.TimeField (null=True, blank=True)
    endTime = models.TimeField (null=True, blank=True)
    duration = models.IntegerField (null=True, blank=True)
    date = models.DateField (null=True, blank=True)
    premadeTask = models.BooleanField (default=False)
    autoMakeTask = models.BooleanField (default=False)
    completed = models.BooleanField (default=False)
    notes = models.CharField (null=True, blank=True, max_length=1000)
    slug = models.SlugField (max_length=300, unique=True, blank=True, null=True)

    @classmethod
    def create (cls, facility):
        employeetask = cls (facility=facility)
        return employeetask
    
    def __str__(self):
        return self.title

    def get_mark_completed_url (self):
        return reverse ('employee-task-mark-completed', kwargs={'slug': self.slug})

    def get_absolute_url (self):
        return reverse ('employee-home')


# Facility Owned Objects ///////////////////////////////////////////////////////////////////////////////////////////////////
class Kennel (models.Model):
    facility = models.ForeignKey (User, on_delete=models.CASCADE, related_name='facilityOfKennel')
    kennelNum = models.IntegerField ()
    dateLastUsed = models.DateField ()
    currentlyOccupied = models.BooleanField (default=False)
    currentReservation = models.ForeignKey (BoardingReservation, on_delete=models.DO_NOTHING, related_name='currentKennelOccupant', blank=True, null=True)

