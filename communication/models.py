from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
from django.conf import settings

#////////////////////////////////////////////////////////////////////////////////////////////////
class AdminMessage (models.Model):
    facility = models.ForeignKey (User, on_delete=models.DO_NOTHING, related_name='facilityOfMessage')
    message = models.TextField (max_length=1000, null=True, blank=True)
    slug = models.SlugField (max_length=300, unique=True, blank=True, null=True)
    read = models.ManyToManyField (settings.AUTH_USER_MODEL, blank=True, related_name='read')

    def __str__(self):
        return self.dog.nickName

    def get_absolute_url (self):
        return reverse ('facility-home')