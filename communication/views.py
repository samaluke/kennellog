from django.shortcuts import render
from .models import AdminMessage
from django.views.generic import RedirectView, CreateView
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
import uuid
from django.utils.text import slugify

#////////////////////////////////////////////////////////////////////////////////////////////////////////////
class ReadMessage (RedirectView):
    def get_redirect_url (self, *args, **kwargs):
        slug = self.kwargs.get("slug")
        obj = get_object_or_404 (AdminMessage, slug=slug)
        url_ = obj.get_absolute_url ()
        user = self.request.user
        if user.is_authenticated:
            if user not in obj.read.all ():
                obj.kudos.add (user)
        return url_

class AdminMessageCreateView (LoginRequiredMixin, CreateView):
    model = AdminMessage
    fields = ['message']
    template = 'communication/adminmessage_form.html'

    def form_valid (self, form):
        form.instance.facility = self.request.user
        form.instance.slug = slugify (form.instance.facility.username + uuid.uuid4().hex[:6].upper())
        return super ().form_valid (form)