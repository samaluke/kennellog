from django.urls import path
from . import views
from django.contrib.auth.models import User
from .views import mainEmployeePage, mainFacilityPage, employeeDogPanel, EmpDashDogDetailView, employeeAdminPanel, employeeReviewReservationsPanel, BoardingResApprovalRedirect, EmployeeTaskCompleteRedirect, employeeTaskPanel, TaskCreateView, TaskUpdateView, TaskDeleteView, employeePlaygroundView
from communication.views import AdminMessageCreateView

urlpatterns = [
    path ('employee-home/', mainEmployeePage, name='employee-home'),
    path ('employee-playground-view/', employeePlaygroundView, name='employee-playground-view'),
    path ('employee-dog/', employeeDogPanel, name='employee-dog'),
    path ('employee-dog/<slug:slug>/', EmpDashDogDetailView.as_view(), name='employee-dog-detail'),
    path ('employee-admin/review-reservations/', employeeReviewReservationsPanel, name='employee-admin-review-reservations'),
    path ('employee-admin/review-reservations/<slug:slug>/approve', BoardingResApprovalRedirect.as_view(), name='board-res-approval'),
    path ('employee-tasks/', employeeTaskPanel, name='employee-tasks'),
    path ('employee-task-create/', TaskCreateView.as_view(), name='employee-task-create-view'),
    path ('employee-tasks/<slug:slug>/edit/', TaskUpdateView.as_view (), name='employee-task-edit'),
    path ('employee-tasks/<slug:slug>/mark-complete', EmployeeTaskCompleteRedirect.as_view (), name='employee-task-mark-completed'),
    path ('employee-tasks/<slug:slug>/delete/', TaskDeleteView.as_view (), name='employee-task-delete'),
    path ('facility-home/', mainFacilityPage, name='facility-home'),
    path ('add-facility-admin-message/', AdminMessageCreateView.as_view(), name='create-admin-message'),
]