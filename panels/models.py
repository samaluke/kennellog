from django.db import models
from django.contrib.auth.signals import user_logged_in
from django.shortcuts import render, get_object_or_404
from scheduling.models import EmployeeTask, BoardingReservation
from datetime import date, datetime, timedelta

def assign_tasks (sender, user, request, **kwargs):
    if user.profile.employee_role == 0:
        walkTasks = EmployeeTask.objects.filter (date=date.today ()).filter (facility=user.profile.facilityWorksAt).filter (title__contains='Walk')
        playTasks = EmployeeTask.objects.filter (date=date.today ()).filter (facility=user.profile.facilityWorksAt).filter (title__contains='Playtime with')

        activeBoardingReservations = BoardingReservation.objects.filter (startDate__lte=date.today()).filter (endDate__gte=date.today()).filter (facility=user.profile.facilityWorksAt).filter (approved=True)
        morningTime = datetime.strptime ("08:00:00", '%H:%M:%S')
        afternoonTime = datetime.strptime ("12:00:00", '%H:%M:%S')
        eveningTime = datetime.strptime ("19:00:00", '%H:%M:%S')

        for reservation in activeBoardingReservations:
            pottyTasksToday = EmployeeTask.objects.filter (reservationBoarding=reservation).filter (date=date.today ()).filter (title__contains='break')

            if pottyTasksToday.count() == 3:
                task1 = pottyTasksToday [0]
                task2 = pottyTasksToday [1]
                task3 = pottyTasksToday [2]
                task1.startTime = morningTime
                task1.endTime = morningTime + timedelta(minutes=5)
                task1.employee = user
                morningTime = task1.endTime
                task1.save ()
                task2.startTime = afternoonTime
                task2.endTime = afternoonTime + timedelta(minutes=5)
                task2.employee = user
                afternoonTime = task2.endTime
                task2.save ()
                task3.startTime = eveningTime
                task3.endTime = eveningTime + timedelta(minutes=5)
                task3.employee = user
                eveningTime = task3.endTime
                task3.save ()
            elif pottyTasksToday.count() == 1:
                if date.today ()==reservation.startDate:
                    task1 = pottyTasksToday [0]
                    task1.startTime = eveningTime
                    task1.endTime = eveningTime + timedelta(minutes=5)
                    task1.employee = user
                    morningTime = task1.endTime
                    task1.save ()
                if date.today ()==reservation.endDate:
                    task1 = pottyTasksToday [0]
                    task1.startTime = morningTime
                    task1.endTime =  + timedelta(minutes=5)
                    task1.employee = user
                    morningTime = task1.endTime
                    task1.save ()

        startSchedulingAt = datetime.strptime ("09:00:00", '%H:%M:%S')

        for task in playTasks:
            task.startTime = startSchedulingAt
            task.endTime = timedelta(minutes=10) + startSchedulingAt
            task.employee = user
            startSchedulingAt = timedelta(minutes=10) + startSchedulingAt
            task.save ()

        startSchedulingAt = datetime.strptime ("16:00:00", '%H:%M:%S')

        for task in walkTasks:
            task.startTime = startSchedulingAt
            task.endTime = startSchedulingAt + timedelta(minutes=15)
            task.employee = user
            startSchedulingAt = task.endTime
            task.save ()
    

user_logged_in.connect (assign_tasks)