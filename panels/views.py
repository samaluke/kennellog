from django.shortcuts import render, get_object_or_404
from user.models import DogProfile
from scheduling.models import (
    DaycareReservation,
    BoardingReservation,
    createBoardCheckInTask,
    createBoardPrepCheckInTask,
    createBoardFeedTasks,
    createBoardWalkTask,
    createBoardPottyBreakTasks,
    createBoardPlayTask,
    createBoardPrepCheckOutTask,
    createBoardCheckOutTask,
    createBoardCleanTask,
    createDaycareCheckInTask,
    createDaycareCheckOutTask,
    EmployeeTask,
    Kennel,
    Playground,
)
from django.contrib.auth.models import User
from user.models import Profile
from communication.models import AdminMessage
from datetime import date, datetime
from django.views.generic import DetailView, UpdateView, RedirectView, CreateView, DeleteView
from django.utils.text import slugify
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
import uuid

#////////////////////////////////////////////////////////////////////////////////////////////////////////////
def mainEmployeePage (request):
    facility = request.user.profile.facilityWorksAt
    halfNum = Kennel.objects.filter (facility=facility).count ()/2
    kennels = Kennel.objects.filter (facility=facility)
    current = datetime.now().strftime("%H:%M:%S")
    currentTime = datetime.strptime (current, "%H:%M:%S")

    tasksToday = EmployeeTask.objects.filter (date=date.today ()).filter (facility=request.user.profile.facilityWorksAt).filter (employeeRole=request.user.profile.employee_role)
    for task in tasksToday:
        task.employee = request.user
        task.save ()

    for kennel in kennels:
        passedTasks = EmployeeTask.objects.filter (reservationBoarding=kennel.currentReservation).filter (date=date.today ()).filter (startTime__lte=datetime.now()).filter (completed=False)
        for task in passedTasks:
            taskHour = task.startTime.strftime ("%H")
            taskMinute = task.startTime.strftime ("%M")
            time = taskHour + ":" + taskMinute + ":00"
            timeStart = datetime.strptime (time, '%H:%M:%S')
            difference = currentTime - timeStart
            if difference.seconds > 900: # fifteen minutes
                kennel.currentReservation.status = 2
                kennel.currentReservation.save ()
            elif difference.seconds > 300: # five minutes
                kennel.currentReservation.status = 1
                kennel.currentReservation.save ()
    
    unreadMessages = False
    messages = AdminMessage.objects.filter (facility=facility)
    #if request.user not in messages.read.all ():
    #    unreadMessages=True
    context = {
        'title': 'Employee Dashboard',
        'tasks': EmployeeTask.objects.filter (employee=request.user).filter (date=date.today ()).order_by ('startTime'),
        'kennels': Kennel.objects.filter (facility=facility).order_by ("kennelNum"),
        'numKennels': Kennel.objects.filter (facility=facility).count (),
        'half': Kennel.objects.filter (facility=facility).count ()/2,
        'rangeOne': Kennel.objects.filter (facility=facility).filter (kennelNum__lte=halfNum),
        'rangeTwo': Kennel.objects.filter (facility=facility).filter (kennelNum__gte=halfNum+1),
        'kennelWrangler': Profile.objects.filter (facilityWorksAt=facility).filter (employee_role=4),
        'unreadMessages': unreadMessages
    }
    return render (request, 'panels/employee_mainpage.html', context)

def employeePlaygroundView (request):
    facility = request.user.profile.facilityWorksAt
    reservations = DaycareReservation.objects.filter (dropoffTime__lte=datetime.now()).filter (pickupTime__gte=datetime.now()).filter (facility=facility).filter (approved=True)
    context = {
        'title': 'Employee Playground View',
        'tasks': EmployeeTask.objects.filter (employee=request.user).filter (date=date.today ()).order_by ('startTime'),
        'playgrounds': Playground.objects.filter (facility=facility),
        'numPlaygrounds': Playground.objects.filter (facility=facility).count (),
        'reservations': reservations,
    }
    return render (request, 'panels/employee_playground_view.html', context)

def employeeDogPanel (request):
    facilityFilt = request.user.profile.facilityWorksAt
    activeBoardingReservations = BoardingReservation.objects.filter (startDate__lte=date.today()).filter (endDate__gte=date.today()).filter (facility=facilityFilt).filter (approved=True)
    activeDaycareReservations = DaycareReservation.objects.filter (dropoffTime__lte=datetime.now()).filter (pickupTime__gte=datetime.now()).filter (facility=facilityFilt).filter (approved=True)
    tasks = EmployeeTask.objects.filter (employee=request.user).filter (date=date.today ()).order_by ('startTime')
    context = {
        'title': 'Dog Panel',
        'boardingdogs': activeBoardingReservations,
        'daycaredogs': activeDaycareReservations,
        'facility': facilityFilt,
        'tasks': tasks,
    }
    return render (request, 'panels/employee_dog_panel.html', context)

def employeeAdminPanel (request):
    context = {
        'title': 'Admin Panel',
        'tasks': EmployeeTask.objects.filter (employee=request.user).filter (date=date.today ()).order_by ('startTime'),
    }
    return render (request, 'panels/employee_admin_panel.html', context)

class EmpDashDogDetailView (DetailView):
    model = DogProfile
    template = 'panels/employee_dog_detail_view.html'
    def get_context_data (self, **kwargs):
        context = super (EmpDashDogDetailView, self).get_context_data(**kwargs)
        return context

def employeeReviewReservationsPanel (request):
    facilityFilt = request.user.profile.facilityWorksAt
    requestedBoardingReservations = BoardingReservation.objects.filter (facility=facilityFilt).filter (approved=False)
    upcomingReservations = BoardingReservation.objects.filter (startDate__gte=date.today()).filter (facility=facilityFilt).filter (approved=True)
    context = {
        'title': 'Approve Reservations',
        'boardingReservations': requestedBoardingReservations,
        'upcomingReservations': upcomingReservations,
        'tasks': EmployeeTask.objects.filter (employee=request.user).filter (date=date.today ()).order_by ('startTime'),
    }
    return render (request, 'panels/employee_review_res_list.html', context)

class DaycareResApprovalRedirect (RedirectView):
    def get_redirect_url (self, *args, **kwargs):
        slug = self.kwargs.get ("slug")
        obj = get_object_or_404 (DaycareReservation, slug=slug)
        obj.approved = True
        createDaycareCheckInTask (obj)
        createDaycareCheckOutTask (obj)
        obj.save ()
        url_ = obj.get_absolute_url ()
        return url_


class BoardingResApprovalRedirect (RedirectView):
    def get_redirect_url (self, *args, **kwargs):
        slug = self.kwargs.get("slug")
        obj = get_object_or_404 (BoardingReservation, slug=slug)
        allKennels = Kennel.objects.filter (facility=obj.facility).order_by ('dateLastUsed')
        firstKennel = allKennels.first ()
        #looking = True

        #while (looking):
        #    firstKennel = allKennels.first ()
        #    firstKenReservations = BoardingReservation.objects.filter (facility=obj.facility).filter (kennelNum=firstKennel.kennelNum)
        #    obj.kennelNum = firstKennel.kennelNum
        #    for reservation in firstKenReservations:
        #        print (reservation)
        #        looking = False

    

        obj.kennelNum = firstKennel.kennelNum
        obj.approved=True
        #createBoardPrepCheckInTask (obj) #borked
        createBoardCheckInTask (obj)
        createBoardFeedTasks (obj)
        createBoardWalkTask (obj) # assign time with employee, check start/end date time
        createBoardPottyBreakTasks (obj)
        createBoardPlayTask (obj)
        #createBoardPrepCheckOutTask (obj) #borked
        createBoardCheckOutTask (obj)
        #createBoardCleanTask (obj) #borked
        obj.save ()
        firstKennel.save ()
        url_ = obj.get_absolute_url ()
        return url_


class TaskCreateView (LoginRequiredMixin, CreateView):
    model = EmployeeTask
    fields = ['employee', 'reservationDaycare', 'reservationBoarding', 'employeeRole', 'title', 'description', 'startTime', 'endTime', 'duration', 'date', 'notes', 'slug']

    def from_valid (self, form):
        form.instance.slug = slugify (form.instance.title + form.instance.employee.profile.username + uuid.uuid4().hex[:6].upper())
        form.instance.facility = form.instance.employee.profile.facilityWorksAt
        return super ().form_valid (form)

class TaskUpdateView (LoginRequiredMixin, UpdateView):
    model = EmployeeTask
    fields = ['employee', 'reservationDaycare', 'reservationBoarding', 'employeeRole', 'title', 'description', 'startTime', 'endTime', 'duration', 'date', 'notes', 'slug']

    def from_valid (self, form):
        form.instance.facility = form.instance.employee.profile.facilityWorksAt
        return super ().form_valid (form)

    def get_success_url(self):
        return '/panels/employee-tasks/'

class EmployeeTaskCompleteRedirect (RedirectView):
    def get_redirect_url (self, *args, **kwargs):
        slug = self.kwargs.get ("slug")
        obj = get_object_or_404 (EmployeeTask, slug=slug)
        kennel = get_object_or_404 (Kennel, facility=obj.reservationBoarding.facility, kennelNum=obj.reservationBoarding.kennelNum)
        if "Check in" in obj.title:
            kennel.currentlyOccupied = True
            kennel.currentReservation = obj.reservationBoarding
            kennel.dateLastUsed = obj.reservationBoarding.endDate
            kennel.save ()
        if "Check out" in obj.title:
            kennel.currentlyOccupied = False
            kennel.save ()
        kennel.currentReservation.status = 0
        kennel.currentReservation.save ()
        obj.completed = True
        obj.save ()
        url_ = obj.get_absolute_url ()
        return url_

def employeeTaskPanel (request):
    context = {
        'title': 'Employee Tasks',
        'tasks': EmployeeTask.objects.filter (employee=request.user).filter (date=date.today ()).order_by ('startTime'),
    }
    return render (request, 'panels/employee_task_panel.html', context)


class TaskDeleteView (LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = EmployeeTask
    success_url = '/panels/employee-tasks/'

    def test_func (self):
        return True

#///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
def mainFacilityPage (request):
    context = {
        'title': 'Facility Dashboard',
    }
    return render (request, 'panels/facility_mainpage.html', context)