from django.contrib import admin
from .models import Profile, HoursAvailable, EmergencyContact, DogProfile

admin.site.register(Profile)
admin.site.register (HoursAvailable)
admin.site.register (DogProfile)