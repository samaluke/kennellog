from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import gettext_lazy as _
from PIL import Image
from django.urls import reverse

#///////////////////////////////////////////////////////////////////////
USER_TYPES = (
    (0,"Facility"),
    (1,"Employee"),
    (2,"Customer"),
)

USER_PRONOUNS = (
    (0,"they/them"),
    (1,"she/her"),
    (2,"he/him"),
)

WEEKDAYS = (
    (0,_("Sunday")),
    (1,_("Monday")),
    (2,_("Tuesday")),
    (3,_("Wednesday")),
    (4,_("Thursday")),
    (5,_("Friday")),
    (6,_("Saturday")),
)

DOG_GENDERS = (
    (0,"Female, Intact"),
    (1,"Male, Intact"),
    (2,"Female, Spayed"),
    (3,"Male, Neutered"),
)

EMPLOYEE_TASK_ROLES = (
    (0,"Dog Exerciser"),
    (1,"Management"),
    (2,"Trainer"),
    (3,"Playtime Handler"),
    (4,"Kennel Wrangler"),
    (5,"No Role"),
)


#///////////////////////////////////////////////////////////////////////
class Profile (models.Model):
    user = models.OneToOneField (User, on_delete=models.CASCADE)
    user_type = models.IntegerField (choices=USER_TYPES, default=0)
    slug = models.SlugField (max_length=200, unique=True)
    birthdate = models.DateField (null=True)
    pronouns = models.IntegerField (choices=USER_PRONOUNS, default=0)
    biography = models.TextField (max_length=1000, null=True, blank=True)
    address = models.CharField (max_length=500, null=True, blank=True)
    phone_number = models.CharField (max_length=20, null=True, blank=True)
    profile_image = models.ImageField (default='default.jpg', upload_to='profile_pics')
    banner_image = models.ImageField (default='defaultbanner.jpg', upload_to='user_gallery') # for facility
    first_name = models.CharField (max_length=200, null=True, blank=True)
    last_name = models.CharField (max_length=200, null=True, blank=True)
    facilityWorksAt = models.ForeignKey (User, default=None, blank=True, null=True, related_name='facility', on_delete=models.CASCADE)
    numOfKennels = models.IntegerField (null=True, blank=True)
    numOfPlaygrounds = models.IntegerField (null=True, blank=True)
    employee_role = models.IntegerField (choices=EMPLOYEE_TASK_ROLES, default=5)

    def __str__(self):
        return f'{self.user.username} Profile'

    def save (self, *args, **kwargs):
        super().save(*args, **kwargs)

        img = Image.open (self.profile_image.path)

        if img.height > 300 or img.width > 300:
            output_size = (300, 300)
            img.thumbnail (output_size)
            img.save (self.profile_image.path)
        
        #if user_type == 0:
            # todo: create kennels for num of kennels


class HoursAvailable (models.Model):
    business = models.ForeignKey (User, on_delete=models.CASCADE)
    weekday = models.IntegerField (choices=WEEKDAYS)
    from_hour = models.TimeField ()
    to_hour = models.TimeField ()

    """
    class Meta:
        ordering = ('weekday', 'from_hour')
        unique_together = ('weekday', 'from_hour', 'to_hour')
    
    def __unicode__ (self):
        return u'%s: %s - %s' % (self.get_weekday_display(),
                                 self.from_hour, self.to_hour)
    """

class EmergencyContact (models.Model):
    user = models.ForeignKey (User, on_delete=models.CASCADE)
    first_name = models.CharField (max_length=200)
    last_name = models.CharField (max_length=200)
    phone_number = models.CharField (max_length=20)
    relationship = models.CharField (max_length=100)

class DogProfile (models.Model):
    owner = models.ForeignKey (User, on_delete=models.CASCADE, related_name='dogs')
    slug = models.SlugField (max_length=200, unique=True, default="dog")
    fullName = models.CharField (max_length=400)
    nickName = models.CharField (max_length=300)
    image = models.ImageField (default='default.jpg', upload_to='profile_pics')
    gender = models.IntegerField (choices=DOG_GENDERS)
    birthdate = models.DateField (null=True)
    breed = models.CharField (max_length=400)
    weight = models.IntegerField ()
    foodBrand = models.CharField (max_length=400, blank=True)
    microchipNum = models.IntegerField (blank=True, null=True)
    feedTime1 = models.TimeField (auto_now=False, blank=True, null=True)
    feedAmt1 = models.IntegerField (blank=True, null=True)
    feedTime2 = models.TimeField (auto_now=False, blank=True, null=True)
    feedAmt2 = models.IntegerField (blank=True, null=True)
    feedTime3 = models.TimeField (auto_now=False, blank=True, null=True)
    feedAmt3 = models.IntegerField (blank=True, null=True)

    def __str__ (self):
        return self.nickName
    
    def get_absolute_url (self):
        return reverse ('dog-detail', kwargs={'slug': self.slug})
