# Generated by Django 3.1.4 on 2021-04-06 03:17

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('user', '0002_dogprofile_image'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='facilityWorksAt',
            field=models.ManyToManyField(blank=True, related_name='employee', to=settings.AUTH_USER_MODEL),
        ),
    ]
