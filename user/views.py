from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from .models import Profile, DogProfile
from .forms import UserRegisterForm, UserUpdateForm, FacilityProfileUpdateForm, EmployeeProfileUpdateForm, CustomerUpdateForm
from django.views.generic import DetailView, CreateView, UpdateView
from django.http import Http404
from django.utils.text import slugify


def register (request):
    if request.method == 'POST':
        form = UserRegisterForm (request.POST)
        if form.is_valid ():
            user = form.save ()
            user.profile.user_type = form.cleaned_data.get ('user_type')
            user.profile.slug = form.cleaned_data.get ('username')
            user.save ()
            username = form.cleaned_data.get('username')
            messages.success (request, f'Account created for {username}! You are now able to log in.')
            return redirect ('login')
    else:
        form = UserRegisterForm ()
    return render (request, 'user/register.html', {'form': form})

class ProfileDetail (DetailView):
    model = Profile
    template = 'user/profile.html'
    def get_context_data (self, **kwargs):
        context = super(ProfileDetail, self).get_context_data(**kwargs)
        context['dogs'] = DogProfile.objects.filter (owner=self.object.user)
        context['coworkers'] = Profile.objects.filter (facilityWorksAt=self.object.user.profile.facilityWorksAt).exclude (user=self.object.user)
        return context
    
    def form_valid (self, form):
        form.instance.slug = slugify (self.request.user.username)
        return super ().form_valid (form)

@login_required
def profile (request):
    if request.method == 'POST':
        u_form = UserUpdateForm (request.POST, instance=request.user)
        p_form = FacilityProfileUpdateForm (request.POST, request.FILES, instance=request.user.profile)
        
        if u_form.is_valid () and p_form.is_valid ():
            u_form.save ()
            p_form.save ()
            messages.success (request, f'Account updated!')
            return redirect ('profile')

    else:
        u_form = UserUpdateForm (instance=request.user)
        p_form = FacilityProfileUpdateForm (instance=request.user.profile)

    context = {
        'u_form': u_form,
        'p_form': p_form
    }
    return render(request, 'user/profile_update.html', context)


class DogProfileDetail (DetailView):
    model = DogProfile
    template = 'user/dog_profile.html'
    def get_context_data (self, **kwargs):
        context = super (DogProfileDetail, self).get_context_data(**kwargs)
        return context
    
    def form_valid (self, form):
        form.instance.slug = slugify (self.request.user.username + " " + form.instance.nickname)
        return super ().form_valid (form)

class DogCreateView (LoginRequiredMixin, CreateView):
    model = DogProfile
    fields = ['owner', 'image', 'slug', 'fullName', 'nickName', 'gender', 'birthdate', 'breed', 'weight', 'foodBrand', 'microchipNum', 'feedTime1', 'feedAmt1', 'feedTime2', 'feedAmt2', 'feedTime3', 'feedAmt3']

    def from_valid (self, form):
        form.instance.slug = slugify (self.request.user.username + " " + form.instance.nickname)
        form.instance.owner = self.request.user
        return super ().form_valid (form)

class DogProfileUpdateView (LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = DogProfile
    fields = ['image', 'fullName', 'nickName', 'gender', 'birthdate', 'breed', 'weight', 'foodBrand', 'microchipNum', 'feedTime1', 'feedAmt1', 'feedTime2', 'feedAmt2', 'feedTime3', 'feedAmt3']

    def from_valid (self, form):
        form.instance.slug = slugify (self.request.user.username + " " + form.instance.nickname)
        return super ().form_valid (form)

    def test_func (self):
        dog = self.get_object()
        if self.request.user == dog.owner:
            return True
        return False