from django.test import TestCase, Client
from django.utils.timezone import now
from scheduling.models import BoardingReservation
from django.urls import reverse
from parameterized import parameterized
import datetime

class BoardingReservationModelTest(TestCase):
    def test_event_title_char_field(self):
        title1 = BoardingReservation.objects.create(title='This is the title', description='Descrition 1', start_time=now(), end_time=now())
        self.assertEqual('This is the title', title1.title)

    def test_event_description_field_value(self):
        text= BoardingReservation.objects.create(title='title', description='Descrition 1', start_time=now(), end_time=now())
        self.assertEqual('Descrition 1', text.description)

    def test_event_start_datetime_field_value(self):
        starttime = BoardingReservation.objects.create(title='title', description='Descrition 1', start_time=datetime.datetime(2020, 11, 11), end_time=now())
        day = datetime.datetime(2020, 11, 11)
        self.assertEqual(day, starttime.start_time)
