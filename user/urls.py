from .views import ProfileDetail, DogProfileDetail, DogProfileUpdateView, DogCreateView
from scheduling.views import BoardingReservationCreateView, BoardingReservationUpdateView, BoardingReservationDeleteView
from django.urls import path
from django.contrib.auth.models import User

urlpatterns = [
    path ('create-dog/', DogCreateView.as_view (), name='dog-create'),
    path ('dog/<slug:slug>/', DogProfileDetail.as_view (), name='dog-detail,'),
    path ('dog/<slug:slug>/edit/', DogProfileUpdateView.as_view (), name='dog-update'),
    path ('boarding-request/', BoardingReservationCreateView.as_view (template_name='scheduling/boarding_request_form.html'), name='boarding-request'),
    path ('boarding-request/<slug:slug>/edit/', BoardingReservationUpdateView.as_view (), name='boarding-request-update'),
    path ('boarding-request/<slug:slug>/delete/', BoardingReservationDeleteView.as_view (), name='boarding-request-delete'),
    path ('<slug:slug>/', ProfileDetail.as_view (template_name='user/profile_detail.html'), name='user-profile'),
]