from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import Profile


USER_TYPES = (
    (0,"Facility"),
    (1,"Employee"),
    (2,"Customer"),
)

#///////////////////////////////////////////////////////////////////////
class UserRegisterForm (UserCreationForm):
    email = forms.EmailField ()
    user_type = forms.ChoiceField (choices=USER_TYPES, help_text='Account Type')

    class Meta:
        model = User
        fields = ['user_type', 'username', 'email', 'password1', 'password2']

class UserUpdateForm (forms.ModelForm):
    email = forms.EmailField ()

    class Meta:
        model = User
        fields = ['username', 'email']

class FacilityProfileUpdateForm (forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['first_name', 'biography', 'address', 'phone_number', 'profile_image', 'banner_image']

class EmployeeProfileUpdateForm (forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['facilityWorksAt', 'first_name', 'last_name', 'birthdate', 'pronouns', 'biography', 'address', 'phone_number', 'profile_image', 'banner_image']

class CustomerUpdateForm (forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['first_name', 'last_name', 'pronouns', 'biography', 'address', 'phone_number', 'profile_image', 'banner_image']